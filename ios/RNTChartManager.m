//
//  RNTChartManager.m
//  rniosui
//
//  Created by hj on 2018/12/3.
//  Copyright © 2018 Facebook. All rights reserved.
//

// 引用第三方库
#import "YKLineChart.h"
#import <React/RCTViewManager.h>

@interface RNTMapManager : RCTViewManager

@end

YKLineChartView *kChart;

@implementation RNTMapManager

RCT_EXPORT_MODULE()


- (YKLineChartView *)view
{
  // 创建组件
  kChart = [[YKLineChartView alloc] init];
  [kChart setupChartOffsetWithLeft:50 top:10 right:10 bottom:10];
  kChart.gridBackgroundColor = [UIColor whiteColor];
  kChart.borderColor = [UIColor colorWithRed:203/255.0 green:215/255.0 blue:224/255.0 alpha:1.0];
  kChart.borderWidth = .5;
  kChart.candleWidth = 8;
  kChart.candleMaxWidth = 30;
  kChart.candleMinWidth = 1;
  kChart.uperChartHeightScale = 0.7;
  kChart.xAxisHeitht = 25;
  kChart.highlightLineShowEnabled = YES;
  kChart.zoomEnabled = YES;
  kChart.scrollEnabled = YES;
  // 返回组件
  return kChart;
}


RCT_EXPORT_METHOD(setData:(NSArray *)sourceArray)
{
  
  // 传入数组
  NSMutableArray * array = [NSMutableArray array];
  
  NSLog(@"arr:%@",sourceArray);

  for (NSDictionary * dic in sourceArray) {
    
    YKLineEntity * entity = [[YKLineEntity alloc]init];
    entity.high = [dic[@"high_px"] doubleValue];
    entity.open = [dic[@"open_px"] doubleValue];
    
    entity.low = [dic[@"low_px"] doubleValue];
    
    entity.close = [dic[@"close_px"] doubleValue];
    
    entity.date = dic[@"date"];
    entity.ma5 = [dic[@"avg5"] doubleValue];
    entity.ma10 = [dic[@"avg10"] doubleValue];
    entity.ma20 = [dic[@"avg20"] doubleValue];
    entity.volume = [dic[@"total_volume_trade"] doubleValue];
    [array addObject:entity];
  }

  
  [array addObjectsFromArray:array];
  NSLog(@"array:%@",array);

  // 设置图表
  YKLineDataSet * dataset = [[YKLineDataSet alloc]init];
  dataset.data = array;
  dataset.highlightLineColor = [UIColor colorWithRed:60/255.0 green:76/255.0 blue:109/255.0 alpha:1.0];
  dataset.highlightLineWidth = 0.7;
  dataset.candleRiseColor = [UIColor colorWithRed:233/255.0 green:47/255.0 blue:68/255.0 alpha:1.0];
  dataset.candleFallColor = [UIColor colorWithRed:33/255.0 green:179/255.0 blue:77/255.0 alpha:1.0];
  dataset.avgLineWidth = 1.f;
  dataset.avgMA10Color = [UIColor colorWithRed:252/255.0 green:85/255.0 blue:198/255.0 alpha:1.0];
  dataset.avgMA5Color = [UIColor colorWithRed:67/255.0 green:85/255.0 blue:109/255.0 alpha:1.0];
  dataset.avgMA20Color = [UIColor colorWithRed:216/255.0 green:192/255.0 blue:44/255.0 alpha:1.0];
  dataset.candleTopBottmLineWidth = 1;
  
  //给图表添加数据
  [kChart setupData:dataset];
}

@end
