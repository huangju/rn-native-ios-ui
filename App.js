/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, NativeModules, Text, View } from 'react-native';
import MapView from './MapView';


export default class App extends Component {
  componentDidMount() {
    console.log('componentDidMount');
    NativeModules.RNTMapManager.setData([{
      "high_px": 120,
      "open_px": 80,
      "low_px": 80,
      "close_px": 100,
      "date": "2015/09/10",
      "avg5": 100,
      "avg10": 100,
      "avg20": 100,
      "total_volume_trade": 100,
    },{
      "high_px": 130,
      "open_px": 100,
      "low_px": 80,
      "close_px": 90,
      "date": "2015/09/10",
      "avg5": 100,
      "avg10": 100,
      "avg20": 100,
      "total_volume_trade": 110,
    },{
      "high_px": 140,
      "open_px": 90,
      "low_px": 70,
      "close_px": 90,
      "date": "2015/09/10",
      "avg5": 100,
      "avg10": 100,
      "avg20": 100,
      "total_volume_trade": 120,
    }])
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <MapView style={{ width: 300, height: 300 }}></MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
