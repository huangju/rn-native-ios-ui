> 参考 https://reactnative.cn/docs/native-components-ios/ （最后封装组件需要给大小才能显示）


### 效果 

![image](https://i.loli.net/2018/12/03/5c054524e8433.png
)

### 大概过程
```
// 引用第三方库
#import "YKLineChart.h"
#import <React/RCTViewManager.h>

@interface RNTMapManager : RCTViewManager
@end

@implementation RNTMapManager

RCT_EXPORT_MODULE()

- (UIView *)view
{
  // 创建组件
  YKLineChartView *kChart = [[YKLineChartView alloc] init];
  
  // 对组件进行加工
  // **** 
  
  // 返回组件
  return kChart;
}

@end
```